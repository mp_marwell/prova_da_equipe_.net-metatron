﻿using System.Collections.Generic;

namespace TODOlistRestDDD.DOMAIN.CORE.Interface.Service
{
    public interface IServiceBase<TEntity> where TEntity : class
    {
        void Add(TEntity obj);

        void Update(TEntity obj);

        void Remove(TEntity obj);

        IEnumerable<TEntity> GetAll();

        TEntity GetById(int Id);
    }
}