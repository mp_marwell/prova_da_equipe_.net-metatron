﻿using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.CORE.Interface.Service
{
    public interface IServiceLista : IServiceBase<Lista>
    {
    }
}