﻿using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.CORE.Interface.Service
{
    public interface IServiceCategoria : IServiceBase<Categoria>
    {
    }
}