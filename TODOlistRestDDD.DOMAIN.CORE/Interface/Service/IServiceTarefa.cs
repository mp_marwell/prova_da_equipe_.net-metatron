﻿using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.CORE.Interface.Service
{
    public interface IServiceTarefa : IServiceBase<Tarefa>
    {
    }
}