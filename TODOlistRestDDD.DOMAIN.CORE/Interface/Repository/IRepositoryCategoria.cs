﻿using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.CORE.Interface.Repository
{
    public interface IRepositoryCategoria : IRepositoryBase<Categoria>
    {
    }
}
