﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.INFRASTRUCTURE.Data.Repository
{
    public class RepositoryLista : RepositoryBase<Lista>, IRepositoryLista
    {
        private readonly SqlContext sqlContext;

        public RepositoryLista(SqlContext sqlContext) : base(sqlContext)
        {
            this.sqlContext = sqlContext;
            
        }
    }
}