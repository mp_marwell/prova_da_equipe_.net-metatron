﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.INFRASTRUCTURE.Data.Repository
{
    public class RepositoryUsuario : RepositoryBase<Usuario>, IRepositoryUsuario
    {
        private readonly SqlContext sqlContext;

        public RepositoryUsuario(SqlContext sqlContext) : base(sqlContext)
        {
            this.sqlContext = sqlContext;
        }
    }
}