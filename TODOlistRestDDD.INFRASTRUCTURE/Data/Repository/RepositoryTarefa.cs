﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.INFRASTRUCTURE.Data.Repository
{
    internal class RepositoryTarefa : RepositoryBase<Tarefa>, IRepositoryTarefa
    {
        private readonly SqlContext sqlContext;

        public RepositoryTarefa(SqlContext sqlContext) : base(sqlContext)
        {
            this.sqlContext = sqlContext;
        }
    }
}