﻿using Microsoft.EntityFrameworkCore;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.INFRASTRUCTURE.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext()
        {
        }

        public SqlContext(DbContextOptions<SqlContext> options) : base(options)
        {
        }

        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Categoria> Categorias { get; set; }
        public DbSet<Lista> Listas { get; set; }
        public DbSet<Tarefa> Tarefas { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseInMemoryDatabase("InMemoryProvider");
        }
        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}