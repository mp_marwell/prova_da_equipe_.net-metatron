﻿using Autofac;
using TODOlistRestDDD.APPLICATION;
using TODOlistRestDDD.APPLICATION.Interface;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.APPLICATION.Mapper;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;
using TODOlistRestDDD.DOMAIN.SERVICE;
using TODOlistRestDDD.INFRASTRUCTURE.Data.Repository;

namespace TODOlistRestDDD.INFRASTRUCTURE.CrossCutting.IOC
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            #region IOC

            builder.RegisterType<ApplicationServiceUsuario>().As<IApplicationServiceUsuario>();
            builder.RegisterType<ApplicationServiceCategoria>().As<IApplicationServiceCategoria>();
            builder.RegisterType<ApplicationServiceLista>().As<IApplicationServiceLista>();
            builder.RegisterType<ApplicationServiceTarefa>().As<IApplicationServiceTarefa>();

            builder.RegisterType<ServiceUsuario>().As<IServiceUsuario>();
            builder.RegisterType<ServiceCategoria>().As<IServiceCategoria>();
            builder.RegisterType<ServiceLista>().As<IServiceLista>();
            builder.RegisterType<ServiceTarefa>().As<IServiceTarefa>();

            builder.RegisterType<RepositoryUsuario>().As<IRepositoryUsuario>();
            builder.RegisterType<RepositoryCategoria>().As<IRepositoryCategoria>();
            builder.RegisterType<RepositoryLista>().As<IRepositoryLista>();
            builder.RegisterType<RepositoryTarefa>().As<IRepositoryTarefa>();

            builder.RegisterType<MapperUsuario>().As<IMapperUsuario>();
            builder.RegisterType<MapperCategoria>().As<IMapperCategoria>();
            builder.RegisterType<MapperLista>().As<IMapperLista>();
            builder.RegisterType<MapperTarefa>().As<IMapperTarefa>();

            #endregion IOC
        }
    }
}