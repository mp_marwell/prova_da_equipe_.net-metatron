﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;

namespace TODOlistRestDDD.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ListasController : ControllerBase
    {
        private readonly IApplicationServiceLista applicationServiceLista;

        public ListasController(IApplicationServiceLista applicationServiceLista)
        {
            this.applicationServiceLista = applicationServiceLista;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(applicationServiceLista.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(applicationServiceLista.GetById(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] ListaDto listaDTO)
        {
            try
            {
                if (listaDTO == null)
                    return NotFound();

                applicationServiceLista.Add(listaDTO);
                return Ok("Lista Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] ListaDto listaDTO)
        {
            try
            {
                if (listaDTO == null)
                    return NotFound();

                applicationServiceLista.Update(listaDTO);
                return Ok("Lista Atualizado com sucesso!");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete()]
        public ActionResult Delete([FromBody] ListaDto listaDTO)
        {
            try
            {
                if (listaDTO == null)
                    return NotFound();

                applicationServiceLista.Remove(listaDTO);
                return Ok("Lista Removido com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}