﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;

namespace TODOlistRestDDD.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UsuariosController : ControllerBase
    {
        private readonly IApplicationServiceUsuario applicationServiceUsuario;

        public UsuariosController(IApplicationServiceUsuario applicationServiceUsuario)
        {
            this.applicationServiceUsuario = applicationServiceUsuario;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(applicationServiceUsuario.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(applicationServiceUsuario.GetById(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] UsuarioDto usuarioDTO)
        {
            try
            {
                if (usuarioDTO == null)
                    return NotFound();

                applicationServiceUsuario.Add(usuarioDTO);
                return Ok("Usuario Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] UsuarioDto usuarioDTO)
        {
            try
            {
                if (usuarioDTO == null)
                    return NotFound();

                applicationServiceUsuario.Update(usuarioDTO);
                return Ok("Usuario Atualizado com sucesso!");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete()]
        public ActionResult Delete([FromBody] UsuarioDto usuarioDTO)
        {
            try
            {
                if (usuarioDTO == null)
                    return NotFound();

                applicationServiceUsuario.Remove(usuarioDTO);
                return Ok("Usuario Removido com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}