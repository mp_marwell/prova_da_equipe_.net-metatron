﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;

namespace TODOlistRestDDD.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class TarefasController : ControllerBase
    {
        private readonly IApplicationServiceTarefa applicationServiceTarefa;

        public TarefasController(IApplicationServiceTarefa applicationServiceTarefa)
        {
            this.applicationServiceTarefa = applicationServiceTarefa;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(applicationServiceTarefa.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(applicationServiceTarefa.GetById(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] TarefaDto tarefaDTO)
        {
            try
            {
                if (tarefaDTO == null)
                    return NotFound();

                applicationServiceTarefa.Add(tarefaDTO);
                return Ok("Tarefa Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] TarefaDto tarefaDTO)
        {
            try
            {
                if (tarefaDTO == null)
                    return NotFound();

                applicationServiceTarefa.Update(tarefaDTO);
                return Ok("Tarefa Atualizado com sucesso!");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete()]
        public ActionResult Delete([FromBody] TarefaDto tarefaDTO)
        {
            try
            {
                if (tarefaDTO == null)
                    return NotFound();

                applicationServiceTarefa.Remove(tarefaDTO);
                return Ok("Tarefa Removido com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}