﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;

namespace TODOlistRestDDD.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class CategoriasController : ControllerBase
    {
        private readonly IApplicationServiceCategoria applicationServiceCategoria;

        public CategoriasController(IApplicationServiceCategoria applicationServiceCategoria)
        {
            this.applicationServiceCategoria = applicationServiceCategoria;
        }

        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(applicationServiceCategoria.GetAll());
        }

        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(applicationServiceCategoria.GetById(id));
        }

        [HttpPost]
        public ActionResult Post([FromBody] CategoriaDto categoriaDTO)
        {
            try
            {
                if (categoriaDTO == null)
                    return NotFound();

                applicationServiceCategoria.Add(categoriaDTO);
                return Ok("Categoria Cadastrado com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [HttpPut]
        public ActionResult Put([FromBody] CategoriaDto categoriaDTO)
        {
            try
            {
                if (categoriaDTO == null)
                    return NotFound();

                applicationServiceCategoria.Update(categoriaDTO);
                return Ok("Categoria Atualizado com sucesso!");
            }
            catch (Exception)
            {
                throw;
            }
        }

        [HttpDelete()]
        public ActionResult Delete([FromBody] CategoriaDto categoriaDTO)
        {
            try
            {
                if (categoriaDTO == null)
                    return NotFound();

                applicationServiceCategoria.Remove(categoriaDTO);
                return Ok("Categoria Removido com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}