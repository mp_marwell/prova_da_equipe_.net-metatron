﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.SERVICE
{
    public class ServiceLista : ServiceBase<Lista>, IServiceLista
    {
        private readonly IRepositoryLista repositoryLista;

        public ServiceLista(IRepositoryLista repositoryLista) : base(repositoryLista)
        {
            this.repositoryLista = repositoryLista;
        }
    }
}