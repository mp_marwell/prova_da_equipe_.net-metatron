﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.SERVICE
{
    public class ServiceTarefa : ServiceBase<Tarefa>, IServiceTarefa
    {
        private readonly IRepositoryTarefa repositoryTarefa;

        public ServiceTarefa(IRepositoryTarefa repositoryTarefa) : base(repositoryTarefa)
        {
            this.repositoryTarefa = repositoryTarefa;
        }
    }
}