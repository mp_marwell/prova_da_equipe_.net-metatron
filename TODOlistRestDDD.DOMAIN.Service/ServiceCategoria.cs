﻿using TODOlistRestDDD.DOMAIN.CORE.Interface.Repository;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.DOMAIN.SERVICE
{
    public class ServiceCategoria : ServiceBase<Categoria>, IServiceCategoria
    {
        private readonly IRepositoryCategoria repositoryCategoria;

        public ServiceCategoria(IRepositoryCategoria repositoryCategoria) : base(repositoryCategoria)
        {
            this.repositoryCategoria = repositoryCategoria;
        }
    }
}