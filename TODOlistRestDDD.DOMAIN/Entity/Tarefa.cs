﻿namespace TODOlistRestDDD.DOMAIN.Entity
{
    public class Tarefa : Base
    {
        public bool Concluida { get; set; }
        public Usuario Usuario { get; set; }
        public int UsuarioId { get; set; }
        public Lista Lista { get; set; }
        public int ListaId { get; set; }
    }
}