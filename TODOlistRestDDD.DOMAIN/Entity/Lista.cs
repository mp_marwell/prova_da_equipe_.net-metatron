﻿namespace TODOlistRestDDD.DOMAIN.Entity
{
    public class Lista : Base
    {
        public Categoria Categoria { get; set; }
        public int CategoriaId { get; set; }
        public bool Concluida { get; set; }
    }
}