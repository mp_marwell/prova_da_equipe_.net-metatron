﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Interface.Mapper
{
    public interface IMapperLista
    {
        Lista MapperDtoToEntity(ListaDto listaDto);

        IEnumerable<ListaDto> MapperListListaDto(IEnumerable<Lista> lista);

        ListaDto MapperEntityToDto(Lista lista);
    }
}