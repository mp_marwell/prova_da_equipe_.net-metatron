﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Interface.Mapper
{
    public interface IMapperCategoria
    {
        Categoria MapperDtoToEntity(CategoriaDto categoriaDto);

        IEnumerable<CategoriaDto> MapperListCategoriaDto(IEnumerable<Categoria> categoria);

        CategoriaDto MapperEntityToDto(Categoria categoria);
    }
}