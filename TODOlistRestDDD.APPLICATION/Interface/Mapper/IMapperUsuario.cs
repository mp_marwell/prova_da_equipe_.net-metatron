﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Interface.Mapper
{
    public interface IMapperUsuario
    {
        Usuario MapperDtoToEntity(UsuarioDto usuarioDto);

        IEnumerable<UsuarioDto> MapperListUsuarioDto(IEnumerable<Usuario> usuario);

        UsuarioDto MapperEntityToDto(Usuario usuario);
    }
}