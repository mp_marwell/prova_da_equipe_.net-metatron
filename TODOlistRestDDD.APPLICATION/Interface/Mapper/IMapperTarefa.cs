﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Interface.Mapper
{
    public interface IMapperTarefa
    {
        Tarefa MapperDtoToEntity(TarefaDto tarefaDto);

        IEnumerable<TarefaDto> MapperListTarefaDto(IEnumerable<Tarefa> tarefa);

        TarefaDto MapperEntityToDto(Tarefa tarefa);
    }
}