﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;

namespace TODOlistRestDDD.APPLICATION.Interface
{
    public interface IApplicationServiceUsuario
    {
        void Add(UsuarioDto usuarioDto);

        void Update(UsuarioDto usuarioDto);

        void Remove(UsuarioDto usuarioDto);

        IEnumerable<UsuarioDto> GetAll();

        UsuarioDto GetById(int Id);
    }
}