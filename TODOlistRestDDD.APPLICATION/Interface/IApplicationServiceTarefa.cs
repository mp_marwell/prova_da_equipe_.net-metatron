﻿using System;
using System.Collections.Generic;
using System.Text;
using TODOlistRestDDD.APPLICATION.DTO;

namespace TODOlistRestDDD.APPLICATION.Interface
{
    public interface IApplicationServiceTarefa
    {
        void Add(TarefaDto TarefaDto);

        void Update(TarefaDto TarefaDto);

        void Remove(TarefaDto TarefaDto);

        IEnumerable<TarefaDto> GetAll();

        TarefaDto GetById(int Id);
    }
}
