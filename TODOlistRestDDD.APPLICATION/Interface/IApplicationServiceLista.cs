﻿using System;
using System.Collections.Generic;
using System.Text;
using TODOlistRestDDD.APPLICATION.DTO;

namespace TODOlistRestDDD.APPLICATION.Interface
{
    public interface IApplicationServiceLista
    {
        void Add(ListaDto ListaDto);

        void Update(ListaDto ListaDto);

        void Remove(ListaDto ListaDto);

        IEnumerable<ListaDto> GetAll();

        ListaDto GetById(int Id);
    }
}
