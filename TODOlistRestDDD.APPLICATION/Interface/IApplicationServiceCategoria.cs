﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;

namespace TODOlistRestDDD.APPLICATION.Interface
{
    public interface IApplicationServiceCategoria
    {
        void Add(CategoriaDto CategoriaDto);

        void Update(CategoriaDto CategoriaDto);

        void Remove(CategoriaDto CategoriaDto);

        IEnumerable<CategoriaDto> GetAll();

        CategoriaDto GetById(int Id);
    }
}