﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;

namespace TODOlistRestDDD.APPLICATION
{
    public class ApplicationServiceCategoria : IApplicationServiceCategoria
    {
        private readonly IServiceCategoria serviceCategoria;
        private readonly IMapperCategoria mapperCategoria;

        public ApplicationServiceCategoria(IServiceCategoria serviceCategoria, IMapperCategoria mapperCategoria)
        {
            this.serviceCategoria = serviceCategoria;
            this.mapperCategoria = mapperCategoria;
        }

        public void Add(CategoriaDto categoriaDto)
        {
            var categoria = mapperCategoria.MapperDtoToEntity(categoriaDto);
            serviceCategoria.Add(categoria);
        }

        public IEnumerable<CategoriaDto> GetAll()
        {
            var categoria = serviceCategoria.GetAll();
            return mapperCategoria.MapperListCategoriaDto(categoria);
        }

        public CategoriaDto GetById(int Id)
        {
            var categoria = serviceCategoria.GetById(Id);
            return mapperCategoria.MapperEntityToDto(categoria);
        }

        public void Remove(CategoriaDto categoriaDto)
        {
            var categoria = mapperCategoria.MapperDtoToEntity(categoriaDto);
            serviceCategoria.Remove(categoria);
        }

        public void Update(CategoriaDto categoriaDto)
        {
            var categoria = mapperCategoria.MapperDtoToEntity(categoriaDto);
            serviceCategoria.Update(categoria);
        }
    }
}