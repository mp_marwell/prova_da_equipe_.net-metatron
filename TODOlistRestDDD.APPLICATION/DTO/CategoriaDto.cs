﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TODOlistRestDDD.APPLICATION.DTO
{
    public class CategoriaDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
    }
}
