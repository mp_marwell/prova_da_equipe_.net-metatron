﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TODOlistRestDDD.APPLICATION.DTO
{
    public class TarefaDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public bool Concluida { get; set; }
        public int UsuarioId { get; set; }
        public int ListaId { get; set; }
    }
}
