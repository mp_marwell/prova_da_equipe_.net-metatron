﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TODOlistRestDDD.APPLICATION.DTO
{
    public class ListaDto
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public int CategoriaId { get; set; }
        public bool Concluida { get; set; }
    }
}
