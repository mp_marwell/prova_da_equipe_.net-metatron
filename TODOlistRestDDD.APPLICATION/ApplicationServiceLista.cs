﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;

namespace TODOlistRestDDD.APPLICATION
{
    public class ApplicationServiceLista : IApplicationServiceLista
    {
        private readonly IServiceLista serviceLista;
        private readonly IMapperLista mapperLista;

        public ApplicationServiceLista(IServiceLista serviceLista, IMapperLista mapperLista)
        {
            this.serviceLista = serviceLista;
            this.mapperLista = mapperLista;
        }

        public void Add(ListaDto listaDto)
        {
            var lista = mapperLista.MapperDtoToEntity(listaDto);
            serviceLista.Add(lista);
        }

        public IEnumerable<ListaDto> GetAll()
        {
            var listas = serviceLista.GetAll();
            return mapperLista.MapperListListaDto(listas);
        }

        public ListaDto GetById(int Id)
        {
            var lista = serviceLista.GetById(Id);
            return mapperLista.MapperEntityToDto(lista);
        }

        public void Remove(ListaDto listaDto)
        {
            var lista = mapperLista.MapperDtoToEntity(listaDto);
            serviceLista.Remove(lista);
        }

        public void Update(ListaDto listaDto)
        {
            var lista = mapperLista.MapperDtoToEntity(listaDto);
            serviceLista.Update(lista);
        }
    }
}