﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;

namespace TODOlistRestDDD.APPLICATION
{
    public class ApplicationServiceUsuario : IApplicationServiceUsuario
    {
        private readonly IServiceUsuario serviceUsuario;
        private readonly IMapperUsuario mapperUsuario;
        public ApplicationServiceUsuario(IServiceUsuario serviceUsuario, IMapperUsuario mapperUsuario)
        {
            this.serviceUsuario = serviceUsuario;
            this.mapperUsuario = mapperUsuario;
        }

        public void Add(UsuarioDto usuarioDto)
        {
            var usuario = mapperUsuario.MapperDtoToEntity(usuarioDto);
            serviceUsuario.Add(usuario);
        }

        public IEnumerable<UsuarioDto> GetAll()
        {
            var usuarios = serviceUsuario.GetAll();
            return mapperUsuario.MapperListUsuarioDto(usuarios);
        }

        public UsuarioDto GetById(int Id)
        {
            var usuario = serviceUsuario.GetById(Id);
            return mapperUsuario.MapperEntityToDto(usuario);
        }

        public void Remove(UsuarioDto usuarioDto)
        {
            var usuario = mapperUsuario.MapperDtoToEntity(usuarioDto);
            serviceUsuario.Remove(usuario);
        }

        public void Update(UsuarioDto usuarioDto)
        {
            var usuario = mapperUsuario.MapperDtoToEntity(usuarioDto);
            serviceUsuario.Update(usuario);
        }
    }
}