﻿using System.Collections.Generic;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.CORE.Interface.Service;

namespace TODOlistRestDDD.APPLICATION
{
    public class ApplicationServiceTarefa : IApplicationServiceTarefa
    {
        private readonly IServiceTarefa serviceTarefa;
        private readonly IMapperTarefa mapperTarefa;

        public ApplicationServiceTarefa(IServiceTarefa serviceTarefa, IMapperTarefa mapperTarefa)
        {
            this.serviceTarefa = serviceTarefa;
            this.mapperTarefa = mapperTarefa;
        }
        public void Add(TarefaDto tarefaDto)
        {
            var tarefa = mapperTarefa.MapperDtoToEntity(tarefaDto);
            serviceTarefa.Add(tarefa);
        }

        public IEnumerable<TarefaDto> GetAll()
        {
            var tarefas = serviceTarefa.GetAll();
            return mapperTarefa.MapperListTarefaDto(tarefas);
        }

        public TarefaDto GetById(int Id)
        {
            var tarefa = serviceTarefa.GetById(Id);
            return mapperTarefa.MapperEntityToDto(tarefa);
        }

        public void Remove(TarefaDto tarefaDto)
        {
            var tarefa = mapperTarefa.MapperDtoToEntity(tarefaDto);
            serviceTarefa.Remove(tarefa);
        }

        public void Update(TarefaDto tarefaDto)
        {
            var tarefa = mapperTarefa.MapperDtoToEntity(tarefaDto);
            serviceTarefa.Update(tarefa);
        }
    }
}