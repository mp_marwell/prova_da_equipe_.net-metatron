﻿using System.Collections.Generic;
using System.Linq;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Mapper
{
    public class MapperCategoria : IMapperCategoria
    {
        public Categoria MapperDtoToEntity(CategoriaDto categoriaDto)
        {
            var categoria = new Categoria()
            {
                Id = categoriaDto.Id,
                Nome = categoriaDto.Nome,
            };
            return categoria;
        }

        public CategoriaDto MapperEntityToDto(Categoria categoria)
        {
            var categoriaDto = new CategoriaDto()
            {
                Id = categoria.Id,
                Nome = categoria.Nome,
            };
            return categoriaDto;
        }

        public IEnumerable<CategoriaDto> MapperListCategoriaDto(IEnumerable<Categoria> categoria)
        {
            var dto = categoria.Select(c => new CategoriaDto { Id = c.Id, Nome = c.Nome });
            return dto;
        }
    }
}