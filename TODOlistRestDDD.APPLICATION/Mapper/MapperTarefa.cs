﻿using System.Collections.Generic;
using System.Linq;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Mapper
{
    public class MapperTarefa : IMapperTarefa
    {
        public Tarefa MapperDtoToEntity(TarefaDto tarefaDto)
        {
            var tarefa = new Tarefa()
            {
                Id = tarefaDto.Id,
                Nome = tarefaDto.Nome,
                ListaId = tarefaDto.ListaId,
                UsuarioId = tarefaDto.UsuarioId,
                Concluida = tarefaDto.Concluida
            };
            return tarefa;
        }

        public TarefaDto MapperEntityToDto(Tarefa tarefa)
        {
            var tarefaDto = new TarefaDto()
            {
                Id = tarefa.Id,
                Nome = tarefa.Nome,
                ListaId = tarefa.ListaId,
                UsuarioId = tarefa.UsuarioId,
                Concluida = tarefa.Concluida
            };
            return tarefaDto;
        }

        public IEnumerable<TarefaDto> MapperListTarefaDto(IEnumerable<Tarefa> tarefa)
        {
            var dto = tarefa.Select(t => new TarefaDto { Id = t.Id, Nome = t.Nome, ListaId = t.ListaId, UsuarioId = t.UsuarioId, Concluida = t.Concluida });
            return dto;
        }
    }
}