﻿using System.Collections.Generic;
using System.Linq;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Mapper
{
    public class MapperLista : IMapperLista
    {
        public Lista MapperDtoToEntity(ListaDto listaDto)
        {
            var lista = new Lista()
            {
                Id = listaDto.Id,
                Nome = listaDto.Nome,
                CategoriaId = listaDto.CategoriaId,
                Concluida = listaDto.Concluida
            };
            return lista;
        }

        public ListaDto MapperEntityToDto(Lista lista)
        {
            var listaDto = new ListaDto()
            {
                Id = lista.Id,
                Nome = lista.Nome,
                CategoriaId = lista.CategoriaId,
                Concluida = lista.Concluida
            };
            return listaDto;
        }

        public IEnumerable<ListaDto> MapperListListaDto(IEnumerable<Lista> lista)
        {
            var dto = lista.Select(l => new ListaDto { Id = l.Id, Nome = l.Nome, CategoriaId = l.CategoriaId, Concluida = l.Concluida });
            return dto;
        }
    }
}