﻿using System.Collections.Generic;
using System.Linq;
using TODOlistRestDDD.APPLICATION.DTO;
using TODOlistRestDDD.APPLICATION.Interface.Mapper;
using TODOlistRestDDD.DOMAIN.Entity;

namespace TODOlistRestDDD.APPLICATION.Mapper
{
    public class MapperUsuario : IMapperUsuario
    {
        public Usuario MapperDtoToEntity(UsuarioDto usuarioDto)
        {
            var usuario = new Usuario()
            {
                Id = usuarioDto.Id,
                Nome = usuarioDto.Nome,
                Email = usuarioDto.Email
            };
            return usuario;
        }

        public UsuarioDto MapperEntityToDto(Usuario usuario)
        {
            var usuarioDto = new UsuarioDto()
            {
                Id = usuario.Id,
                Nome = usuario.Nome,
                Email = usuario.Email
            };
            return usuarioDto;
        }

        public IEnumerable<UsuarioDto> MapperListUsuarioDto(IEnumerable<Usuario> usuario)
        {
            var dto = usuario.Select(u => new UsuarioDto { Id = u.Id, Nome = u.Nome, Email = u.Email });
            return dto;
        }
    }
}